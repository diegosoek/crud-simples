@extends('layouts.master')
 
@section('content')

<a href="{{ URL::route('categories') }}" class="btn btn-primary">Categorias</a>

<a href="{{ URL::route('books') }}" class="btn btn-primary">Livros</a>

@endsection