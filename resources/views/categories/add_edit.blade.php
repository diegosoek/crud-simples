@extends('layouts.master')

@section('content')

{!! Form::open(['class' => 'form-horizontal']) !!}

    <fieldset>

        <legend>Categoria</legend>
        <!-- Email -->
        <div class="form-group">
            {!! Form::label('name', 'Nome:', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-10">
                {!! Form::text('name', (!isset($model) ? $value = null : $value = $model->name), ['class' => 'form-control', 'placeholder' => 'Nome']) !!}
            </div>
        </div>
        {!! Form::hidden('id', (!isset($model) ? $value = null : $value = $model->id), ['class' => 'form-control', 'placeholder' => 'Nome']) !!}
        <!-- Submit Button -->
        <div class="form-group">
            <div class="col-lg-10 col-lg-offset-2">
                {!! Form::submit('Submit', ['class' => 'btn btn-lg btn-info pull-right'] ) !!}
            </div>
        </div>
    </fieldset>

{!! Form::close()  !!}
@endsection