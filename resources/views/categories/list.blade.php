@extends('layouts.master')
 
@section('content')
 
<h3>Categorias</h3>
<div class="list-group">
    @if(count($categorias) == 0)
        Sem dados
    @endif
    @foreach($categorias as $categoria)
        <a class="list-group-item clearfix" onclick="window.location.href='{{ URL::route('categories_edit', $categoria->id) }}'">
            {{$categoria->name}}
            <span class="pull-right">
                <span class="btn btn-xs btn-default" onclick="window.location.href='{{ URL::route('categories_remove', $categoria->id) }}'; event.stopPropagation();">
                    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                </span>
            </span>
        </a>
    @endforeach
</div>
<a href="{{ URL::route('categories_add') }}" class="btn btn-primary">Adicionar</a>
@endsection