@extends('layouts.master')
 
@section('content')
 
<h3>Livros</h3>
<div class="list-group">
    @if(count($books) == 0)
        Sem dados
    @endif
    @foreach($books as $book)
        <a class="list-group-item clearfix" onclick="window.location.href='{{ URL::route('books_edit', $book->id) }}'">
            {{$book->title}}
            <span class="pull-right">
                <span class="btn btn-xs btn-default" onclick="window.location.href='{{ URL::route('books_remove', $book->id) }}'; event.stopPropagation();">
                    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                </span>
            </span>
        </a>
    @endforeach
</div>
<a href="{{ URL::route('books_add') }}" class="btn btn-primary">Adicionar</a>
@endsection