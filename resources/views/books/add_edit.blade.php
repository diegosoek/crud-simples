@extends('layouts.master')

@section('content')

{!! Form::open(['class' => 'form-horizontal']) !!}

    <fieldset>

<legend>Categoria</legend>
 
        <div class="form-group">
            {!! Form::label('isbn', 'ISBN:', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-10">
                {!! Form::text('isbn', (!isset($model) ? $value = null : $value = $model->isbn), ['class' => 'form-control', 'placeholder' => 'isbn']) !!}
            </div>
        </div>
 
        <div class="form-group">
            {!! Form::label('title', 'Titulo:', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-10">
                {!! Form::text('title', (!isset($model) ? $value = null : $value = $model->title), ['class' => 'form-control', 'placeholder' => 'Titulo']) !!}
            </div>
        </div>
 
        <div class="form-group">
            {!! Form::label('description', 'Descrição:', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-10">
                {!! Form::text('description', (!isset($model) ? $value = null : $value = $model->description), ['class' => 'form-control', 'placeholder' => 'Descrição']) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('price', 'Preço:', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-10">
                {!! Form::text('price', (!isset($model) ? $value = null : $value = $model->price), ['class' => 'form-control', 'placeholder' => 'Preço']) !!}
            </div>
        </div>

        <!-- Select With One Default -->
        <div class="form-group">
            {!! Form::label('category_id', 'Categoria', ['class' => 'col-lg-2 control-label'] )  !!}
            <div class="col-lg-10">
                {!! Form::select('category_id', $categories, (!isset($model) ? $value = null : $value = $model->category_id), array('placeholder' => 'Selecione','class' => 'form-control')) !!}
            </div>
        </div>
 
        {!! Form::hidden('id', (!isset($model) ? $value = null : $value = $model->id), ['class' => 'form-control', 'placeholder' => 'Nome']) !!}
        <!-- Submit Button -->
        <div class="form-group">
            <div class="col-lg-10 col-lg-offset-2">
                {!! Form::submit('Submit', ['class' => 'btn btn-lg btn-info pull-right'] ) !!}
            </div>
        </div>
 
    </fieldset>

{!! Form::close()  !!}
@endsection