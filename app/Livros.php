<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Livros extends Model
{
    
    protected $table = 'books';

    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

}
