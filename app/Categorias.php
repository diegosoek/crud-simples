<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categorias extends Model
{

    protected $table = 'categories';

    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    
    public function books()
    {
        return $this->hasMany('App\Livros', 'category_id');
    }

    // this is a recommended way to declare event handlers
    public static function boot() {
        parent::boot();

        static::deleting(function($book) { // before delete() method call this
                $book->books()->delete();
                // do the rest of the cleanup...
        });
    }

}
