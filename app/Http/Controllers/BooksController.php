<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Categorias;
use App\Livros;

class BooksController extends Controller
{
    public function list(){
        $livros = Livros::get();
        $data['books'] = $livros;
        return view('books.list', $data);
    }

    public function add_edit($id = null){
        $data = [];
        $data['categories'] = Categorias::pluck('name', 'id');
        if(isset($id)){
            $model = Livros::find($id);
            $data['model'] = $model;
        }
        return view('books.add_edit', $data);
    }

    public function save(Request $request){
        $model = null;
        if(isset($request->id)){
            $model = Livros::find($request->id);
        }else{
            $model = new Livros();
        }
        $model->isbn = $request->isbn;
        $model->category_id = $request->category_id;
        $model->title = $request->title;
        $model->price = $request->price;
        $model->description = $request->description;
        $model->save();
        return redirect('books');
    }

    public function remove(Request $request, $id){
        $model = Livros::find($request->id);
        $model->delete();
        return redirect('books');
    }

}
