<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Categorias;

class CategoriesController extends Controller
{
    public function list(){
        $categorias = Categorias::get();
        $data['categorias'] = $categorias;
        return view('categories.list', $data);
    }

    public function add_edit($id = null){
        $data = [];
        if(isset($id)){
            $model = Categorias::find($id);
            $data['model'] = $model;
        }
        return view('categories.add_edit', $data);
    }

    public function save(Request $request){
        $model = null;
        if(isset($request->id)){
            $model = Categorias::find($request->id);
        }else{
            $model = new Categorias();
        }
        $model->name = $request->name;
        $model->save();
        return redirect('categories');
    }

    public function remove(Request $request, $id){
        $model = Categorias::find($request->id);
        $model->delete();
        return redirect('categories');
    }

}
