<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Route::prefix('categories')->group(function () {
    Route::get('/', 'CategoriesController@list')->name('categories');
    Route::get('/add', 'CategoriesController@add_edit')->name('categories_add');
    Route::post('/add', 'CategoriesController@save');
    Route::get('/edit/{id}', 'CategoriesController@add_edit')->name('categories_edit');
    Route::post('/edit/{id}', 'CategoriesController@save');
    Route::get('/remove/{id}', 'CategoriesController@remove')->name('categories_remove');
});

Route::prefix('books')->group(function () {
    Route::get('/', 'BooksController@list')->name('books');
    Route::get('/add', 'BooksController@add_edit')->name('books_add');
    Route::post('/add', 'BooksController@save');
    Route::get('/edit/{id}', 'BooksController@add_edit')->name('books_edit');
    Route::post('/edit/{id}', 'BooksController@save');
    Route::get('/remove/{id}', 'BooksController@remove')->name('books_remove');
});